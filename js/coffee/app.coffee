'use strict'

app = angular.module 'csfd', ["ngRoute"]

app.config(["$routeProvider", ($routeProvider)->

    $routeProvider
    .when("/", {
                templateUrl: "partials/home.html"
                controller: "indexCtrl"
            })
    .when("/movie/:id", {
                templateUrl: "partials/detail.html"
                controller: "movieCtrl"
            })
    .otherwise({
                redirectTo : "/"
            })
    return
])

app.controller("indexCtrl", ["$scope", "$http", ($scope, $http)->

    $scope.search = ()->
        $http.get("http://csfdapi.cz/movie?search=" + encodeURIComponent($scope.movieName))
            .success((data)->
                $scope.data = data
                return
            )
        return

])

app.controller("movieCtrl", ["$scope", "$http", "$routeParams", ($scope, $http, $routeParams)->

    $http.get("http://csfdapi.cz/movie/" + $routeParams.id)
        .success((data)->
            $scope.data = data
            return
        )

])